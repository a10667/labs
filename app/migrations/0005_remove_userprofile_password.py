# Generated by Django 4.0.10 on 2023-12-27 23:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_answer_question'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='password',
        ),
    ]
