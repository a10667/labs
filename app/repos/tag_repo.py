
from app.models import QuestionTags
from app.repos.question_tag_repo import question_tag_get

def filter_questions_by_tag(tag):
    tagobj =question_tag_get(tag)
    ls = []
    for i in QuestionTags.objects.all().filter(question_tag=tagobj):
        ls.append(i.question)
    return ls

