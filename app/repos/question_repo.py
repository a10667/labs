from app.models import Question


def question_get(pk):
    return Question.objects.get(id=pk)

def question_get_all():
    return Question.objects.all()
