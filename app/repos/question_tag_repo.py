from app.models import QuestionTag

def question_tag_get_all():
    return QuestionTag.objects.all();

def question_tag_get(tag):
    return QuestionTag.objects.get(value=tag);

def question_tag_get_all_with_tag(tag):
    return QuestionTag.objects.all().filter(value=tag);
