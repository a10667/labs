
from datetime import datetime
from django.contrib.auth.models import User

from django.shortcuts import redirect, render

from django.contrib.auth import authenticate, login, logout
from django import forms
from django.contrib import messages

from app.models import UserProfile
from app.repos.question_repo import question_get_all
from app.repos.question_tag_repo import question_tag_get_all
from app.repos.users_repo import user_profile_get_all

def add_new_user(request):
    if request.method == 'POST':
        ulogin = request.POST['login']
        upassword = request.POST['password']
        e = request.POST['email']
        nickname = request.POST['nickname']
        user = authenticate(request, username=ulogin, password=upassword)
        if user is None:
            u = User.objects.create_user(username=ulogin, password=upassword)
            up = UserProfile(user=u, email=e,nickname=nickname,registration_date=datetime.now())
            u.save()
            up.save()
            return redirect("login")
        else:
            messages.success(request, "User exist")
    return render(request, "signup.html", {"tags":question_tag_get_all(), "bestmembers":user_profile_get_all() })
