
from django.shortcuts import redirect, render

from django.contrib.auth import authenticate, login, logout
from django import forms
from django.contrib import messages

from app.repos.question_repo import question_get_all
from app.repos.question_tag_repo import question_tag_get_all
from app.repos.users_repo import user_profile_get_all

def logUser(request):
    if request.method == 'POST':
        ulogin = request.POST['login']
        upassword = request.POST['password']
        user = authenticate(request, username=ulogin, password=upassword)
        if user is not None:
            login(request, user)
            return redirect("index")
        else:
            messages.error(request, "nope")
    return render(request, "login.html", {"tags": question_tag_get_all(), "bestmembers":user_profile_get_all() })
