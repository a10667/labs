from app.models import Question, UserProfile


def user_profile_get_all():
    return UserProfile.objects.all()

def user_profile_get_by_question(id):
    q = Question.objects.get(id=id)
    a = UserProfile.objects.get(id=q.author.id)
    return a 
