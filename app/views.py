from datetime import datetime
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import redirect, render
from app.controllers.index_controller import IndexController
from app.controllers.question_controller import QuestionController
from app.controllers.tag_controller import TagController
from app.repos.login_repo import logUser
from app.repos.question_repo import question_get, question_get_all
from app.repos.question_tag_repo import question_tag_get
from app.repos.question_tag_repo import question_tag_get_all
from app.repos.signup_repo import add_new_user
from app.repos.tag_repo import filter_questions_by_tag
from app.repos.users_repo import user_profile_get_all, user_profile_get_by_question
from .models import Question, QuestionTag, QuestionTags, UserProfile

from django.core.paginator import Paginator

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm 
from django import forms
from django.contrib import messages

# Create your views here.

def render_with_pagination(object_list, request, template, context):
    paginator = Paginator(object_list, 6)
    page_number = request.GET.get('page') 
    context["pages"] = paginator.get_page(page_number)
    return render(request, template, context)



def index(request):
    context = IndexController.context
    qs = Question.objects.all()
    return render_with_pagination(qs, request, "index.html", context)

def login_user(request):
    return logUser(request)    

def logout_user(request):
    logout(request)
    messages.success(request, ("You have been logged out"))
    return redirect('index')

def settings(request):
    context = IndexController.context
    return render(request, "settings.html", context)

def signup(request):
    return add_new_user(request)

def ask(request):
    return render(request, "ask.html", {"tags":question_tag_get_all(), "bestmembers": user_profile_get_all() })

def question(request, pk):
    context = QuestionController.context
    q = question_get(pk)
    context["current"] = q 
    context['author']  = user_profile_get_by_question(id=q.id)
    return render(request, "question.html", context)

def tag(request, tag):
    context = TagController.context
    context["questions"] = filter_questions_by_tag(tag)
    return render(request, "tag.html", context)
