from django.urls import path
from .views import ask, index, login_user, logout_user, question, signup, tag, settings

urlpatterns = [
    path('', index, name="index"),
    path('login/', login_user, name="login"),
    path('logout/', logout_user, name="logout"),
    path('settings/',settings, name="settings"),
    path('sigup/', signup, name="signup"),
    path('ask/', ask, name="ask"),
    path('question/<int:pk>',question, name="question"),
    path('tag/<str:tag>', tag, name="tag"),
]
