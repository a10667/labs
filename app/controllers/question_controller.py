from app.repos.question_repo import question_get_all
from app.repos.question_tag_repo import question_tag_get_all
from app.repos.users_repo import user_profile_get_all

class QuestionController():
    context = {
        "tags": question_tag_get_all(), 
        "bestmembers": user_profile_get_all(),
        "current": None

    }
