from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class QuestionTags(models.Model):
    id           = models.AutoField(primary_key=True)
    question     = models.ForeignKey('Question', on_delete=models.CASCADE)
    question_tag = models.ForeignKey('QuestionTag', on_delete=models.CASCADE)

    def __str__(self):
        return "%s: %s" % (self.question, self.question_tag)

class QuestionTag(models.Model):
    id      = models.AutoField(primary_key=True, unique=True)
    value   = models.CharField(max_length=200)

    def __str__(self):
        return self.value 


class Question(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    author = models.ForeignKey('UserProfile', on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=255)
    contents = models.TextField()
    date = models.DateTimeField()
    raito = models.FloatField(default=0)

    @property
    def author_avatar(self):
        return self.author.avatar

    @property
    def author_has_avatar(self):
        return self.author.has_avatar()

    @property
    def answers(self):
        return Answer.objects.all().filter(question=self)

    @property
    def answers_count(self):
        return len(Answer.objects.all().filter(question=self))

    @property
    def description(self):
        if len(self.contents) >= 65:
            return self.contents[:65] + "..."
        else:
            return self.contents

    @property
    def tags(self):
        return QuestionTags.objects.all().filter(question=self)

    def __str__(self):
        return "%s" % self.title 

class UserProfile(models.Model):
    user  = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.CharField(max_length=255)
    nickname = models.CharField(max_length=255)
    avatar = models.ImageField(default="/default.jpg")
    registration_date = models.DateTimeField()
    raito = models.FloatField(default=0)

    @property
    def has_avatar(self):
        return self.avatar != 0 or self.avatar != None

    def __str__(self):
        return self.nickname 

class Answer(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    author = models.ForeignKey('UserProfile', on_delete=models.DO_NOTHING)
    contents = models.TextField()
    date = models.DateTimeField()
    is_correct = models.BooleanField()
    raito = models.FloatField()

    def __str__(self):
        return "anwer on %s" % self.question

