from django.contrib import admin
from .models import Answer, Question, QuestionTag, QuestionTags, UserProfile

# Register your models here.

admin.site.register(QuestionTag)
admin.site.register(Question)
admin.site.register(UserProfile)
admin.site.register(Answer)
admin.site.register(QuestionTags)
