asgiref==3.7.2
beautifulsoup4==4.12.2
Django==4.0.10
Pillow==10.1.0
soupsieve==2.5
sqlparse==0.4.4
